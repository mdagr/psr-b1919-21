# PSR B1919+21

Recreation of an [iconic album cover](https://en.wikipedia.org/wiki/Unknown_Pleasures) using [R](https://www.r-project.org/).

[![PSR B1919+21](results/psr-b1919-21.png)](https://en.wikipedia.org/wiki/PSR_B1919%2B21)

The cover of *Unknown Pleasures* by Joy Division features the plot of the signal coming from the pulsar PSR B1919+21 (CP 1919) made by Harold Craft for his [thesis](https://static.scientificamerican.com/blogs/assets/sa-visual/Image/fig537.jpg).

Dataset from [outside this world](https://gist.githubusercontent.com/borgar/31c1e476b8e92a11d7e9/raw/0fae97dab6830ecee185a63c1cee0008f6778ff6/pulsar.csv). Seemingly [approximate data](https://bl.ocks.org/borgar/31c1e476b8e92a11d7e9) sourced from a vector image by [Michael Zoellner](http://michaelzoellner.de/projects/unknown-pleasures-3d-print) ? See also [another R version](https://github.com/coolbutuseless/CP1919) and [a dataviz historical investigation](https://adamcap.com/2011/05/19/the-history-of/)...
